(function(){
    angular
        .module("UserRegisterApp")
        .controller("UserRegisterCtrl", [UserRegisterCtrl]);

    UserRegisterCtrl.$inject = [];
    function UserRegisterCtrl() {
        var vm = this;

        vm.User = {

            username: "",
            email: "",
            dateOfBirth: "",
            gender: "",
            tel: "",
            address: "",
            country: "",
            pw: ""
        };
        // Triggered when register button clicked

        vm.register = register;
        vm.isAgeValid = isAgeValid;
        vm.submitFunction = submitFunction;

        /////
        function register() {
            console.info("register click");
            console.info("username: %s", vm.username);
            console.info("email: %s", vm.email);
            console.info("dateOfBirth: %s", vm.dateOfBirth);
            console.info("gender: %s", vm.gender);
            console.info("tel: %s", vm.tel);
            console.info("address: %s", vm.address);
            console.info("country: %s", vm.country);
            console.info("password: %s", vm.pw);

            $http.post("/register", {params: {
                username: vm.username,
                email: vm.email,
                gender: vm.gender

            }})


        } //

        function isAgeValid() {

            var date = new Date(vm.dateOfBirth);

            if (date.getFullYear() < 1998) {
                return true;
            }

        }

        function submitFunction() {
            document.getElementById("submit").innerHTML = "You have submitted the form successfully, Thank you! ";
        }


    }// End UserRegisterCtrl
})();