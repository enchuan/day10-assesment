/**
 * Created by mac on 21/10/16.
 */
var express=require('express');
var app=express();

var PORT = 3000;

app.use(express.static(__dirname + '/../client'));

app.get("/register", function(req, res) {
    var username = req.query.username;
    var email = req.query.email;
    var gender = req.query.gender;
    console.info("username: %s\nemail: %s\ngender: %s", username, email, gender);

    res.status(200).end()
});



app.listen(PORT, function(){
    console.log("Server is listening at port: "+PORT);
});